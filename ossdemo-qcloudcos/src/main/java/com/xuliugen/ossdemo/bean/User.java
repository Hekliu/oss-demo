package com.xuliugen.ossdemo.bean;

/**
 * Created by xuliugen on 16/5/22.
 */
public class User {

    private Long id;

    private String name;

    private String headImg;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
