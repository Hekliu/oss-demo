package com.xuliugen.ossdemo.service;

import com.xuliugen.ossdemo.common.domain.RequestData;
import com.xuliugen.ossdemo.dao.DaoSupport;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
@SuppressWarnings("unchecked")
public class UserService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /*
    * 新增
    */
    public void save(RequestData pd) throws Exception {
        dao.save("UserMapper.save", pd);
    }

    /*
    * 删除
    */
    public void delete(RequestData pd) throws Exception {
        dao.delete("UserMapper.delete", pd);
    }

    /*
    * 修改
    */
    public void edit(RequestData pd) throws Exception {
        dao.update("UserMapper.edit", pd);
    }

    /*
    *列表(全部)
    */
    public List<RequestData> listAll(RequestData pd) throws Exception {
        return (List<RequestData>) dao.findForList("UserMapper.listAll", pd);
    }

    /*
    * 通过id获取数据
    */
    public RequestData findById(RequestData pd) throws Exception {
        return (RequestData) dao.findForObject("UserMapper.findById", pd);
    }

    /*
    * 批量删除
    */
    public void deleteAll(String[] ArrayDATA_IDS) throws Exception {
        dao.delete("UserMapper.deleteAll", ArrayDATA_IDS);
    }

}

