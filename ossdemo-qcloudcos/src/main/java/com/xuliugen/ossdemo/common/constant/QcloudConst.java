package com.xuliugen.ossdemo.common.constant;

/**
 * 用于腾讯云COS的常量
 * Created by xuliugen on 16/5/23.
 */
public interface QcloudConst {

    // 默认获取的目录列表的数量
    public int DIR_NUM = 20;

    //默认的远程文件夹路径,这里不用谢bucket name
    public String DEFAULT_REMOTE_FOLDER_PATH = "/demo/";

//    http://xuliugen4student-10041983.file.myqcloud.com/demo/
// d20a38559c3f4981bd0bc765174cdfc2.png?
// sign=
// d/VeTyb8Rm3lURfonTtqjrPSTvlhPTEwMDQxOTgzJms9QUtJRENlWkJITUtXWXlsOHBQSExyN3VjbnRGSkY5elJtUmp0JmU9MTQ2NjYwMDQxMyZ0PTE0NjQwMDg0MTMmcj0xODI3NTczODIyJmY9L2RlbW8vZDIwYTM4NTU5YzNmNDk4MWJkMGJjNzY1MTc0Y2RmYzIucG5nJmI9eHVsaXVnZW40c3R1ZGVudA==
// eB1nzZY4x4fc8z9Soh4i0mjWeRphPTEwMDQxOTgzJms9QUtJRENlWkJITUtXWXlsOHBQSExyN3VjbnRGSkY5elJtUmp0JmU9MTQ2NjYwMDYzMiZ0PTE0NjQwMDg2MzImcj0yMTE3MzM2NTIzJmY9JmI9eHVsaXVnZW40c3R1ZGVudA

}
