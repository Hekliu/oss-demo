package com.xuliugen.ossdemo.common.domain;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 自定义的用于封装request请求参数
 * Created by xuliugen on 16/5/22.
 */
public class RequestData extends HashMap implements Map {

    private static final long serialVersionUID = 1L;

    Map map = null;
    HttpServletRequest request;

    public RequestData(HttpServletRequest request) {
        this.request = request;
        Map returnMap = new HashMap();
        try {
            if (ServletFileUpload.isMultipartContent(request)) {

                DefaultMultipartHttpServletRequest dmRequest = (DefaultMultipartHttpServletRequest) request;
                Enumeration<String> names = dmRequest.getParameterNames();
                while (names.hasMoreElements()) {
                    String name = names.nextElement();
                    if (null != dmRequest.getParameter(name).toString() && (!dmRequest.getParameter(name).toString().isEmpty())) {
                        returnMap.put(name, dmRequest.getParameter(name));
                    }
                }
            } else {
                Enumeration<String> names = request.getParameterNames();
                while (names.hasMoreElements()) {
                    String name = names.nextElement();
                    if (null != request.getParameter(name).toString() && (!request.getParameter(name).toString().isEmpty())) {
                        returnMap.put(name, request.getParameter(name));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map = returnMap;
    }

    public RequestData() {
        map = new HashMap();
    }

    @Override
    public Object get(Object key) {
        Object obj = null;
        if (map.get(key) instanceof Object[]) {
            Object[] arr = (Object[]) map.get(key);
            obj = request == null ? arr
                    : (request.getParameter((String) key) == null ? arr
                    : arr[0]);
        } else {
            obj = map.get(key);
        }
        return obj;
    }

    public String getString(Object key) {
        return (String) get(key);
    }

    public Integer getInteger(Object key) {
        return (Integer) get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        return map.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    public void clear() {
        map.clear();
    }

    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    public Set entrySet() {
        return map.entrySet();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public Set keySet() {
        return map.keySet();
    }

    public void putAll(Map t) {
        map.putAll(t);
    }

    public int size() {
        return map.size();
    }

    public Collection values() {
        return map.values();
    }

}
