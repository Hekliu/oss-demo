<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv='X-UA-Compatible' content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title>文件上传</title>
    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>">
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div>
    <form action="user/save.do" name="Form" id="Form" method="post" enctype="multipart/form-data">
        <div>
            <table id="table_report" class="table table-striped table-bordered table-hover">

                <tr>
                    <td style="width:70px;text-align: right;padding-top: 13px;">名称:</td>
                    <td>
                        <input type="text" name="name" id="nameID"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:70px;text-align: right;padding-top: 13px;">头像:</td>
                    <td><input type="file" name="headImg" id="headImgID"/></td>
                </tr>

                <tr>
                    <td style="text-align: center;" colspan="10">
                        <input class="btn btn-primary" type="submit"/>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
</body>
</html>