package com.xuliugen.ossdemo.utils;

import com.xuliugen.ossdemo.qcloudclient.QiniuClient;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by xuliugen on 16/5/22.
 */
public class UploadFileUtil {

    /**
     * 执行文件上传的
     * @param max_size
     * @param request  request请求
     * @return 文件上传之后存储的路径的url List集合
     * @throws Exception
     */
    public static List uploadFile(long max_size, HttpServletRequest request) throws Exception {
        List fileList = new ArrayList();
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (multipartResolver.isMultipart(request)) {
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            Iterator<String> iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                List<MultipartFile> files = multiRequest.getFiles(iter.next());
                for (int i = 0; i < files.size(); i++) {
                    MultipartFile file = files.get(i);
                    String myFileName = file.getOriginalFilename();
                    if (StringUtils.notEmpty(myFileName)) {
                        Map map = new HashMap();
                        String type = getFileType(myFileName);
                        String fileName = get32UUID() + type;
                        InputStream input = file.getInputStream();
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024];
                        int n = 0;
                        while (-1 != (n = input.read(buffer))) {
                            output.write(buffer, 0, n);
                        }
                        byte[] file_byte = output.toByteArray();
                        //使用七牛API进行文件保存
                        QiniuClient.uploadFile(file_byte, fileName);
                        map.put("file_url", fileName);
                        fileList.add(map);
                    }
                }
            }
        }
        return fileList;
    }

    /**
     * 获取文件类型
     * @param file_name
     * @return
     */
    private static String getFileType(String file_name) {
        int length = file_name.length();
        int potin = file_name.lastIndexOf(".");
        String file_type = "";
        if (potin == -1) {
            if (length > 6) {
                potin = length - 5;
            } else {
                potin = length - 3;
            }
        }
        file_type = file_name.substring(potin, length).toLowerCase();
        return file_type;
    }

    /**
     * 获取UUID 32位
     * @return
     */
    public static String get32UUID() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "");
    }

}
