package com.xuliugen.ossdemo.utils;

/**
 * Created by xuliugen on 16/5/22.
 */
public class StringUtils {

    /**
     * 检测字符串是否不为空(null,"","null")
     * @param str 需要进行判断的字符串
     * @return 不为空则返回true，否则返回false
     */
    public static boolean notEmpty(String str) {
        return str != null && !"".equals(str) && !"null".equals(str);
    }

}
