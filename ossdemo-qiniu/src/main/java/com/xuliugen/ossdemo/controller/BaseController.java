package com.xuliugen.ossdemo.controller;

import com.xuliugen.ossdemo.common.domain.RequestData;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings({"rawtypes", "unchecked"})
public class BaseController {

//    protected Logger logger = Logger.getLogger(this.getClass());

    /**
     * 得到PageData
     */
    public RequestData getRequestData() {
        return new RequestData(this.getRequest());
    }

    /**
     * 当有文件上传的时候,需要传入特定的request以得到PageData
     * @param request 传入request,这是因为再有文件上传的表单中ShiroHttpServletRequest转化为DefaultMultipartHttpServletRequest
     * @return
     */
    public RequestData getRequestData(HttpServletRequest request) {
        return new RequestData(request);
    }

    /**
     * 得到request对象
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }
}
