package com.xuliugen.ossdemo.controller;

import com.xuliugen.ossdemo.common.constant.ConstString;
import com.xuliugen.ossdemo.common.domain.RequestData;
import com.xuliugen.ossdemo.service.UserService;
import com.xuliugen.ossdemo.utils.UploadFileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by xuliugen on 16/5/22.
 */
@Controller
@RequestMapping(value = "/user", produces = {ConstString.APP_JSON_UTF_8})
public class UserController extends BaseController {

    @Resource(name = "userService")
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/save")
    public ModelAndView save(HttpServletRequest request) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        RequestData requestData = super.getRequestData(request);

        List fileList = UploadFileUtil.uploadFile(0, request);
        if (fileList != null && fileList.size() > 0) {
            String file_url = (String) ((Map) fileList.get(0)).get("file_url");
            requestData.put("headImg", file_url);
        }

        userService.save(requestData);
        modelAndView.addObject("result", "success");
        modelAndView.setViewName("result/save_result");
        return modelAndView;
    }
}
